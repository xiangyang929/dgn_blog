# _*_ coding: utf-8 _*_
from django.db import models
from django.contrib.auth.models import User 
from DjangoUeditor.models import UEditorField #头部增加这行代码导入UEditorField


#数据模块，数据库设计就在此文件中设计。

class Fenlei(models.Model):
    name=models.CharField('文章分类',max_length=100)
    class Meta:
        verbose_name='文章分类'
        verbose_name_plural='文章分类'
    def __str__(self):
        return self.name

#文章
class Wenzhang(models.Model):
    title=models.CharField('文章标题',max_length=70)
    wzfenlei=models.ForeignKey(Fenlei,on_delete=models.DO_NOTHING,verbose_name='文章分类',blank=True,null=True)
    wz = UEditorField('内容', width=800, height=500, 
                    toolbars="full", imagePath="upimg/", filePath="upfile/",
                    upload_settings={"imageMaxSize": 1204000},
                    settings={}, command=None, blank=True
                    )
    user=models.ForeignKey(User,on_delete=models.CASCADE, verbose_name='作者')
    views = models.PositiveIntegerField('阅读量', default=0)

    created_time = models.DateTimeField('发布时间', auto_now_add=True)
    modified_time = models.DateTimeField('修改时间', auto_now=True)
    class Meta:
        verbose_name='文章'
        verbose_name_plural='文章'
    def __str__(self):
        return self.title

#Like
class Like(models.Model):

    wz=models.ForeignKey(Wenzhang,on_delete=models.CASCADE,verbose_name='点赞文章ID',blank=True,null=True)
    user=models.CharField('点赞的人',default='',max_length=100,blank=True,null=True)
    def __str__(self):
        return str(self.wz)

#shoucang
class Shoucang(models.Model):
    wz=models.ForeignKey(Wenzhang,on_delete=models.CASCADE,verbose_name='收藏的文章ID')
    user = models.CharField('收藏的人', default='', max_length=100, blank=True, null=True)
    # user=models.ForeignKey(User,related_name='shoucang_user',on_delete=models.DO_NOTHING,verbose_name='收藏的人',blank=True,null=True)
    def __str__(self):
        return str(self.wz)
#Banner
class Banner(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE, verbose_name='作者',blank=True,null=True)
    text_info = models.CharField('标题', max_length=50, default='')
    img = models.ImageField('轮播图(大小必须为820像素*200像素)', upload_to='banner/')
    img_link=models.CharField('点击图片打开的网址',max_length=150,default="/index")
    is_active = models.BooleanField('是否是激活显示', default=True)
    def __str__(self):
        return self.text_info
    class Meta:
        verbose_name = '轮播图'
        verbose_name_plural = '轮播图'

    list_display_links=('text_info')

#友情链接
class Link(models.Model):
    name = models.CharField('链接名称', max_length=20)
    linkurl = models.URLField('网址',max_length=100)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = '友情链接'
        verbose_name_plural = '友情链接'
        
#留言板
class Ly(models.Model):
    name = models.CharField('昵称', max_length=50)
    say = models.CharField('留言',max_length=300)
    created_time = models.DateTimeField('发布时间', auto_now_add=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = '给我留言'
        verbose_name_plural = '给我留言'

#个人简介+联系方式
class Grjj(models.Model):
    nick_name = models.CharField('昵称', max_length=50, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='姓名')
    jj=UEditorField('个人简介', width=800, height=500,
                    toolbars="full", imagePath="upimg/", filePath="upfile/",
                    upload_settings={"imageMaxSize": 1204000},
                    settings={}, command=None, blank=True,null=True
                    )
    tel=models.CharField('电话', max_length=50,blank=True,null=True)
    e_mail=models.EmailField('邮箱地址',blank=True,null=True)
    user_image = models.ImageField('头像',upload_to='user_image',blank=True,null=True)
    modified_time = models.DateTimeField('修改时间', auto_now=True)
    def __str__(self):
        return self.nick_name
    class Meta:
        verbose_name = '个人简介&联系方式'
        verbose_name_plural = '个人简介&联系方式'

#每日一句
class Mryj(models.Model):
    yiju = models.CharField('每日一句', max_length=120)
    created_time = models.DateTimeField('加入时间', auto_now_add=True)
    def __str__(self):
        return self.yiju
    class Meta:
        verbose_name = '每日一句谏言(120字符)'
        verbose_name_plural = '每日一句谏言(120字符)'