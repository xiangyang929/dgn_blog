from django.contrib import admin

# Register your models here.
 
from .models import Banner,Wenzhang,Fenlei, Link ,Ly,Grjj,Mryj
from django.template.context_processors import request
from django.utils.safestring import mark_safe
admin.site.site_header = "攀登的多人博客系统-后台登录"

@admin.register(Wenzhang)
class WenzhangAdmin(admin.ModelAdmin):
 
    def get_queryset(self,request):
        qs = super(WenzhangAdmin,self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)
    
    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()
    
    #详细页面排除字段
    exclude = ('views','user')
    #只读
    readonly_fields = ('user',)
    list_display = ('id','title','wzfenlei', 'user','views', 'created_time')
    list_per_page = 50
    ordering = ('-created_time',)
    list_display_links = ('id', 'title')

@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    def get_queryset(self,request):
        qs = super(BannerAdmin,self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()

    #管理后台显示缩略图
    list_display = ('id', 'text_info', 'img', 'image_data','img_link', 'is_active')
    readonly_fields = ('image_data',)  # 必须加这行 否则访问编辑页面会报错

    #返回HTML代码给前台
    def image_data(self, obj):
        return mark_safe('<img src="%s" width="80px"  height="50px"/>' % obj.img.url)

    # 页面显示的字段名称
    image_data.short_description = '缩略图'

    #详细页面排除字段
    exclude = ('user',)

@admin.register(Fenlei)
class FenleiAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

@admin.register(Link)
class LinkAdmin(admin.ModelAdmin):
    list_display = ('id', 'name','linkurl')

@admin.register(Ly)
class LyAdmin(admin.ModelAdmin):
    list_display = ('name', 'say','created_time')


@admin.register(Grjj)
class GrjjAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        #qs = super(GrjjAdmin,self).get_queryset(request)
        #if request.user.is_superuser:
            #return qs
        return Grjj.objects.filter(user=request.user)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()

    list_display = ( 'nick_name','jj','tel','e_mail','user_image','modified_time')

    # 只读
    readonly_fields = ('user',)
    exclude = ('user',)

@admin.register(Mryj)
class GrjjAdmin(admin.ModelAdmin):
    list_display = ('yiju','created_time')


