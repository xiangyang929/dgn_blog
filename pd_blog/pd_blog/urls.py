"""pd_blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from blog import views 
from django.urls import path, include, re_path
from django.views.static import serve
from django.conf.urls import url, include
from django.views import  static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),  
    path('', views.index),           
    path('base/', views.base),    
    path('index/', views.index),  
    path('hello/',views.hello),
    path('wz_liebiao/', views.wz_liebiao), 
    path('read/', views.read),  
    path('like/', views.like),
    path('search/', views.search),
    path('ly/', views.liuyanban),
    path('wz_gdlb/', views.wz_gdlb),
    path('shbj/<str:s>', views.shbj),
    path('jksh/<str:s>', views.jksh),
    path('grjj/', views.grjj),
    path('dzsc/', views.dzsc),
    path('comments/', include('django_comments.urls')),
    path('accounts/', include('allauth.urls')),
    url('^static/(?P<path>.*)$', static.serve, {'document_root': 'static'}, name='static'),
    path('ueditor/', include('DjangoUeditor.urls')),  
    re_path('^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
]
